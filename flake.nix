{
  description = "An usefull shell library, with nix functions to package shell scripts";

  inputs = {
    # base
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
    systems = {
      url = "github:nix-systems/x86_64-linux"; # only evaluate for this system
    };
    flake-utils = {
      url = "github:numtide/flake-utils";
      inputs = {
        systems.follows = "systems";
      };
    };
    flake_version_update = {
      url = "git+https://codeberg.org/soispha/flake_version_update.git";
      inputs = {
        systems.follows = "systems";
        nixpkgs.follows = "nixpkgs";
        flake-utils.follows = "flake-utils";
      };
    };
  };

  outputs = {
    self,
    nixpkgs,
    flake-utils,
    flake_version_update,
    ...
  }:
    flake-utils.lib.eachDefaultSystem (system: let
      pkgs = nixpkgs.legacyPackages.${system};

      # This version is set automatically on `cog bump --auto`;
      version = "v2.1.2"; # GUIDING VERSION STRING

      cleaned_version = pkgs.lib.strings.removePrefix "v" version;

      mkTest = script:
        pkgs.stdenv.mkDerivation rec {
          name = "${script}";
          src = script;
          dontUnpack = true;
          doCheck = true;
          buildPhase = ''
            touch $out;
          '';
          checkPhase = ''
            file="${src}"
            echo "executing: '$file'"
            . "$file"
            if [ "$?" -ne 0 ]; then
                echo "test ('$file') failed, see above"
                exit 1
            fi
          '';
        };
    in {
      lib = import ./src/lib.nix {
        inherit pkgs;
        inherit (pkgs) lib;
        shell_library = self.rawLib.${system};
      };
      rawLib = "${self.packages."${system}".default}/include/lib.sh";
      rawTemplate = "${self.packages."${system}".default}/template/template.sh";

      apps = {
        default = self.apps."${system}".update_shell_library;
        update_shell_library = {
          type = "app";
          program = "${self.packages.${system}.update_shell_library}/bin/update_shell_library";
        };
      };

      checks = {
        default = self.checks."${system}".shell_code;

        shell_code = self.packages."${system}".shell_library;

        nix_code = let
          script = "${self.lib."${system}".writeShellScript {
            dependencies = with pkgs; [hello];
            name = "nix_code_test";
            src = ./tests/nix/base_test;
            version = "0.1.2";
            replacementStrings = {
              "SOME_STRING_TO_REPLACE" = "TRUE";
            };
            generateCompletions = false;
            keepPath = false;
            wrap = true;
          }}/bin/nix_code_test";
        in
          mkTest script;

        nix_code_version = let
          script = "${self.lib."${system}".writeShellScript {
            dependencies = with pkgs; [hello];
            name = "nix_code_test";
            src = ./tests/nix/base_test;
            replacementStrings = {
              "SOME_STRING_TO_REPLACE" = "TRUE";
            };
            generateCompletions = false;
            keepPath = false;
            wrap = true;
          }}/bin/nix_code_test";
        in
          mkTest script;

        nix_code_multipart = let
          script = "${self.lib."${system}".writeShellScriptMultiPart {
            baseName = "main";
            cmdPrefix = "functions";
            cmdNames = ["a" "b" "c"];
            name = "multipart_test";
            src = ./tests/nix/multi_part_test;
            version = "20.2";
            replacementStrings = {
              "SOME_STRING_TO_REPLACE" = "TRUE";
            };
            generateCompletions = false;
            keepPath = false;
            wrap = true;
          }}/bin/multipart_test";
        in
          mkTest script;
      };

      packages = {
        default = self.packages."${system}".shell_library;
        update_shell_library = pkgs.writeShellApplication {
          name = "update_shell_library";
          runtimeInputs = with pkgs; [fd gnused];
          text = ''
            # the fd part needs to be unquoted, as otherwise sed thinks that's one large
            # filename.
            # shellcheck disable=SC2046
            sed -i 's|SHELL_LIBRARY_VERSION="[0-9]\+\.[0-9]\+\.[0-9]\+"|SHELL_LIBRARY_VERSION="${cleaned_version}"|' $(fd . --type file)
          '';
        };
        shell_library = pkgs.stdenv.mkDerivation {
          pname = "shell_library";
          inherit version;
          src = ./.;

          nativeBuildInputs = with pkgs; [
            pandoc
            fd
          ];

          configurePhase = ''
            sed -i 's|%SHELL_LIBRARY_VERSION|${cleaned_version}|' template/template.sh;

            sed -i 's|%SHELL_LIBRARY_MAJOR_VERSION|${pkgs.lib.versions.major cleaned_version}|' src/shell/99_check_version.sh;
            sed -i 's|%SHELL_LIBRARY_MINOR_VERSION|${pkgs.lib.versions.minor cleaned_version}|' src/shell/99_check_version.sh;
            sed -i 's|%SHELL_LIBRARY_PATCH_VERSION|${pkgs.lib.versions.patch cleaned_version}|' src/shell/99_check_version.sh;
          '';

          buildPhase = ''
            mkdir include;

            sed -i 's|%AWK_CLEAN_FILE|./src/data/10_awk_clean.awk|' src/shell/0_clean.sh; #'

            # compile the shell library
            . src/shell/0_clean.sh;
            tmp="$(mktemp)";
            fd . src/shell --extension sh --type file > "$tmp";

            while read -r source_file; do
              clean "$source_file" >> lib.sh;
            done < "$tmp"
            rm "$tmp";

            # compile the manual
            pandoc docs/shell_library.3.md -s -t man  > shell_library.3
          '';

          doCheck = true;
          nativeCheckInputs = with pkgs; [
            delta
            git # Sort of needed by delta

            # used to test help generation
            hjson
            jq
          ];
          checkPhase = ''
            # Load the shell library
            SHELL_LIBRARY_VERSION="${cleaned_version}" . ./lib.sh
            tmp_correct="$(mktmp)";
            tmp_tested="$(mktmp)";

            msg "Started testing"
            for file in $(fd . ./tests --type file --extension sh)
            do
                msg2 "executing: '$file'"
                . ./"$file"
                delta "$tmp_tested" "$tmp_correct" || die "Test ('$file') failed, see above"
                tmp_correct="$(mktmp)";
                tmp_tested="$(mktmp)";
            done
            msg "Finished testing"
          '';

          installPhase = ''
            install -D shell_library.3 $out/share/man/man3/shell_library.3;
            install -D template/template.sh $out/template/template.sh;
            install -D lib.sh $out/include/lib.sh;

            mkdir $out/data;
            cp --recursive src/data/. $out/data/.;

            sed -i "s|./src/data/10_awk_clean.awk|$out/data/10_awk_clean.awk|" $out/include/lib.sh;
          '';
        };
      };

      devShells.default = pkgs.mkShell {
        packages = with pkgs; [
          cocogitto
          flake_version_update.packages."${system}".default
        ];
      };
    });
}
# vim: ts=2

