# Changelog
All notable changes to this project will be documented in this file. See [conventional commits](https://www.conventionalcommits.org/) for commit guidelines.

- - -
## v2.1.2 - 2024-03-15
#### Bug Fixes
- **(src/clean)** Correctly filter out c like comments - (20fd7c1) - Soispha

- - -

## v2.1.1 - 2024-02-24
#### Bug Fixes
- **(src/lib.nix)** Remove deprecated '--replace' flag - (48d788e) - Soispha
#### Build system
- **(flake)** Update - (3daed7f) - Soispha

- - -

## v2.1.0 - 2024-02-24
#### Build system
- **(flake)** Remove flake_update (it is included in the system closure) - (7585631) - Soispha
#### Documentation
- **(tests)** Document, how to better see the zsh comp output - (5050307) - Soispha
#### Features
- **(src/completions)** Only show options, if the user inputted a '-' - (ba31c46) - Soispha
- **(src/completions)** Add support for file args in help - (73e6f1b) - Soispha
- **(src/lib.nix)** Run dash in dry run mode as check - (94b42d7) - Soispha
#### Miscellaneous Chores
- **(revert)** build(flake): Remove flake_update (it is included in the system closure) - (d8bab7b) - Soispha

- - -

## v2.0.13 - 2024-01-13
#### Bug Fixes
- **(src/lib.nix)** Remove issues with unset version - (3cdd6d3) - Soispha

- - -

## v2.0.12 - 2024-01-13
#### Bug Fixes
- **(src/lib.nix)** Support replacementStrings in the part files - (71074cd) - Soispha
#### Miscellaneous Chores
- **(version)** v2.0.11 - (1cc2630) - Soispha
#### Tests
- **(flake)** Add tests for the nix generation code - (ba69d32) - Soispha

- - -

## v2.0.11 - 2024-01-13
#### Bug Fixes
- **(scr/lib.nix)** Also substitute the input files - (1b3d8f3) - Soispha

- - -

## v2.0.10 - 2024-01-13
#### Bug Fixes
- **(scr/lib.nix)** Don't remove the source command - (bf2b91b) - Soispha

- - -

## v2.0.9 - 2024-01-13
#### Bug Fixes
- **(scr/lib.nix)** Ensure that strings are in one line - (b0e80c6) - Soispha

- - -

## v2.0.8 - 2024-01-13
#### Bug Fixes
- **(scr/lib.nix)** Correctly build up string - (148ce33) - Soispha

- - -

## v2.0.7 - 2024-01-13
#### Bug Fixes
- **(scr/lib.nix)** Don't cat, simply substitute the paths - (a854f28) - Soispha

- - -

## v2.0.6 - 2024-01-13
#### Bug Fixes
- **(scr/lib.nix)** Make everything relative to `src` - (85d0617) - Soispha

- - -

## v2.0.5 - 2024-01-13
#### Bug Fixes
- **(scr/lib.nix)** Use `cat` instead of `builtins.readFile` - (6d915c9) - Soispha

- - -

## v2.0.4 - 2024-01-13
#### Bug Fixes
- **(scr/lib.nix)** Add missing `+` to string concatenation - (8b00688) - Soispha
#### Refactoring
- **(scr/lib.nix)** Use `multi_part` instead of `multi_file` - (e5bd7f7) - Soispha

- - -

## v2.0.3 - 2024-01-13
#### Bug Fixes
- **(scr/lib.nix)** Construct string, then pass it into `runCommand` - (48bd63d) - Soispha
- **(scr/lib.nix)** Only add `makeWrapper` to closure, if it's needed - (37bde51) - Soispha

- - -

## v2.0.2 - 2024-01-13
#### Bug Fixes
- **(scr/lib.nix)** Substitute the multi file part before the replacementStrings - (2c8613c) - Soispha

- - -

## v2.0.1 - 2024-01-13
#### Bug Fixes
- **(scr/lib.nix)** Accept empty dependencies - (e7b1954) - Soispha

- - -

## v2.0.0 - 2024-01-13
#### Bug Fixes
- **(aliases)** Use consistent naming scheme in the dbg aliases - (eb8b0c1) - Soispha
- **(aliases)** Make the dbg enable/disable aliases naming more obvious - (84db7b3) - Soispha
- **(completions/generate_completions.sh)** Remove debug parts - (1e9cfa0) - Soispha
- **(completions/parse)** Use '[[' and ']]' to specify arg-functionsThis was necessary, as the former backticks '`' could not really beescaped in shell code, considering that they were an old version of '$()'.Double brackets can easily be escaped, if the even need to be escaped.BREAKING CHANGE: Simply change the backtick to two square brackets - (f1161c4) - Soispha
- **(exit)** Readd, as this was lost in the refactor - (d107358) - Soispha
- **(flake)** Add lib as an argument - (fb33326) - Soispha
- **(flake)** Add full path to the outputs, not only the dir - (cd5de59) - Soispha
- **(flake)** Add derivation - (a7bf930) - Soispha
- **(flake)** Fix typo - (eb418a9) - Soispha
- **(flake/shell_library_update)** Support version numbers > 10 - (0e09a48) - Soispha
- **(flake/update_shell_library)** Use correct regex - (4a18ab7) - Soispha
- **(lib.nix)** Add fd as dependency - (5290349) - Soispha
- **(lib.sh)** Warn on outdated script - (54b628f) - Soispha
- **(lib/nix)** Add 'gnugrep' as dependency - (c6ddfac) - Soispha
- **(output)** Print correct output, not a line per string - (f89c61b) - Soispha
- **(scr/tempfiles)** Remove wrong deprecation warning - (f689e35) - Soispha
- **(src/lib.nix)** Point the cp to the correct path - (2f0d23c) - Soispha
- **(src/lib.nix)** Substitute after installing - (430593b) - Soispha
- **(src/lib.nix)** Rework baseFunction - (858b115) - Soispha
- **(src/lib.nix)** Improve deprecation notices - (5a80986) - Soispha
- **(src/lib.nix)** Specify missing dependencies - (85b2caa) - Soispha
- **(src/lib.nix)** Rename 'script' argument to 'src' - (5cad957) - Soispha
- **(src/lib.nix)** Rename functions to comply with nixpkgs naming - (cbfb969) - Soispha
- **(src/nix)** Create output dir before using it - (063d4a0) - Soispha
- **(src/nix)** Specify '-r' for cp - (638278d) - Soispha
- **(src/nix)** Use the correct 'lib.optionals' instead of 'lib.optional' - (df1edbe) - Soispha
- **(src/shell/check_versions)** Correctly pass the version string around - (6d053cf) - Soispha
- **(tempfiles)** Don't export the tempdir - (e6ae260) - Soispha
- **(tempfiles)** Check correct variable for spaces - (a129940) - Soispha
- **(tempfiles)** Set the temp dir variable before subshells - (6f9380c) - Soispha
- **(tempfiles/ptmp)** Sanitize input (use %s) - (bca431a) - Soispha
- **(tempfiles/tmp)** Support pipes ("|") - (e48ed12) - Soispha
- **(tempfiles/tmp)** Remove `eval` to avoid word breakage - (b1c97c2) - Soispha
- **(template)** Add vim filetype directive - (841f9ff) - Soispha
- **(treewide)** Replace old `tmp`, and add deprecation notice - (e9c01b9) - Soispha
- **(update)** Remove, as it's handled differently - (906b137) - Soispha
#### Build system
- **(cog)** Run `nix flake check` to ensure correctness - (63982e6) - Soispha
- **(cog)** Rename changelog to CHANGELOG.md - (2207cdd) - Soispha
- **(cog)** Add - (be0c0f3) - Soispha
- **(flake)** Remove lsp's from devshell - (2403028) - Soispha
- **(flake)** Update - (ac22ec1) - Soispha
- **(flake)** Update - (c9dc46c) - Soispha
- **(flake)** Update - (684ac96) - Soispha
- **(flake)** Update - (bf228e3) - Soispha
- **(flake)** Update - (cf69a6f) - Soispha
- **(flake)** Update - (d9ac274) - Soispha
- **(flake)** Update - (c01f5b5) - Soispha
- **(flake)** Update - (ac4e403) - Soispha
- **(flake)** Add automatic versioning of the flake.nix - (e8bfcbd) - Soispha
- **(flake)** Update - (2fd71e2) - Soispha
#### Documentation
- **(clean)** Add comment why the quoting is in fact correct - (4a7f195) - Soispha
- **(lib/nix)** Correct typo in comment - (da8fb30) - Soispha
- **(manpage)** Add - (024198f) - Soispha
- **(src/shell/clean)** Add comment for further reading - (05d4d29) - Soispha
#### Features
- **(clean)** Correct awk source file behavior - (a18506e) - Soispha
- **(completion)** Support arguments for options - (79ccf99) - Soispha
- **(dir_search)** Abstract to allow specified file names - (f3b270c) - Soispha
- **(dir_search)** Add a function to get the flake base dir - (ff84012) - Soispha
- **(flake)** Add app output to automatically update to newest version - (886adad) - Soispha
- **(flake)** Expose raw lib as output - (b5e06c5) - Soispha
- **(lib.sh)** Add version function - (1dacbab) - Soispha
- **(lib.sh)** Allow to pass arguments to `mktemp` - (ab76d79) - Soispha
- **(lib/sh/alias)** Add a function to turn on 'set -x', for full debug - (563259c) - Soispha
- **(scr/lib)** Add further aliases for an easier api - (07f5da1) - Soispha
- **(scr/lib)** Add debug command - (9af89ac) - Soispha
- **(scr/lib.nix)** Add support for multi file scripts - (9ba1252) - Soispha
- **(scr/lib.sh)** Add dbg2 for better selection when debugging - (9e75adc) - Soispha
- **(src/clean)** Add command to correctly quote the input for shells - (8649d03) - Soispha
- **(src/completions)** Init the parsing scripts - (a0e7ff3) - Soispha
- **(src/lib.nix)** Add an option to allow for replacementStrings - (82ae51b) - Soispha
- **(src/lib.nix)** Add the required variables for the version function - (58de28a) - Soispha
- **(src/lib.sh)** Make debug blue - (bb5eacb) - Soispha
- **(src/nix)** Add support for the completion generation - (78ae94d) - Soispha
- **(tempfiles)** Add `tmp_pipe` function - (f792843) - Soispha
- **(tempfiles)** Add a `ptmp` function to support a common pattern - (8cc66d3) - Soispha
- **(template)** Add a template - (855cf80) - Soispha
- **(treewide)** Support version checking - (407c593) - Soispha
- **(treewide)** Update to Nix - (a5d899b) - Soispha
- **(update)** Add script to remember updating - (8a03c1f) - Soispha
#### Miscellaneous Chores
- **(template)** Update year - (e2f7a4b) - Soispha
- **(version)** v1.10.2 - (b734716) - Soispha
- **(version)** v1.10.1 - (a547e31) - Soispha
- **(version)** v1.10.0 - (91a155a) - Soispha
- **(version)** v1.9.0 - (8ea1f02) - Soispha
- **(version)** v1.8.0 - (a42a516) - Soispha
- **(version)** v1.7.1 - (8626e70) - Soispha
- **(version)** v1.7.0 - (0b5212d) - Soispha
- **(version)** v1.6.4 - (f940d7a) - Soispha
- **(version)** v1.6.3 - (efd8447) - Soispha
- **(version)** v1.6.2 - (f279208) - Soispha
- **(version)** v1.6.2 - (fd7c65a) - Soispha
- **(version)** v1.6.1 - (dc33558) - Soispha
- **(version)** v1.6.0 - (7c40716) - Soispha
- **(version)** v1.5.0 - (70b0964) - Soispha
- **(version)** v1.4.2 - (191c8da) - Soispha
- **(version)** v1.4.1 - (ee0db5a) - Soispha
- **(version)** v1.4.0 - (c41b6bf) - Soispha
- **(version)** v1.3.1 - (d3eeefb) - Soispha
- **(version)** v1.3.0 - (cdd8a24) - Soispha
- **(version)** v1.2.0 - (a5bed77) - Soispha
- **(version)** v1.1.4 - (6a63b44) - Soispha
- **(version)** v1.1.3 - (7a38bb4) - Soispha
- **(version)** v1.1.2 - (9868bd6) - Soispha
- **(version)** v1.1.1 - (7895c77) - Soispha
- **(version)** v1.1.0 - (21244a0) - Soispha
- **(version)** v1.0.1 - (96a23a1) - Soispha
- **(version)** v0.5.0 - (8974531) - Soispha
- **(version)** v0.4.2 - (c32f086) - Soispha
- **(version)** v0.4.1 - (306819e) - Soispha
- **(version)** v0.4.0 - (3799001) - Soispha
- **(version)** v0.3.0 - (ce0b09e) - Soispha
- **(version)** v0.2.3 - (bdeff99) - Soispha
- **(version)** v0.2.2 - (5e65a13) - Soispha
- **(version)** v0.2.1 - (237099b) - Soispha
- **(version)** v0.2.0 - (6678638) - Soispha
- **(version)** v0.1.0 - (20e4e37) - Soispha
#### Performance Improvements
- **(src/shell/clean)** Avoid allocation a tempfile - (788e559) - Soispha
#### Refactoring
- **(completions/awk)** Use slashes to encode the regex - (381c271) - Soispha
- **(lib.sh)** Remove main.sh, as it was not used - (8972132) - Soispha
- **(lib.sh)** Separate input from output functions - (0385b36) - Soispha
- **(lib.sh)** Separate shell library in multiple file - (2e169de) - Soispha
- **(src/lib.nix)** Use one function with arguments, instead of three - (7f14127) - Soispha
- **(src/shell)** Sort scripts with numbers - (94a11f4) - Soispha
- **(treewide)** Use consistent naming in internal variables - (3237983) - Soispha
- **(treewide)** Merge lib and scr directory - (7f7dcfa) - Soispha
#### Style
- **(src/lib.nix)** Format - (63f0985) - Soispha
#### Tests
- Add Tests - (3c50031) - Soispha

- - -

## v1.10.2 - 2024-01-01
#### Bug Fixes
- **(flake/shell_library_update)** Support version numbers > 10 - (6b3cd7b) - Soispha

- - -

## v1.10.1 - 2024-01-01
#### Bug Fixes
- **(aliases)** Use consistent naming scheme in the dbg aliases - (9d4fc28) - Soispha
- **(tempfiles)** Don't export the tempdir - (6ed2286) - Soispha
#### Documentation
- **(clean)** Add comment why the quoting is in fact correct - (6434f4d) - Soispha
- **(lib/nix)** Correct typo in comment - (4d520d6) - Soispha
#### Refactoring
- **(completions/awk)** Use slashes to encode the regex - (4dcfe04) - Soispha
- **(treewide)** Use consistent naming in internal variables - (dccb8de) - Soispha

- - -

## v1.10.0 - 2023-12-28
#### Build system
- **(flake)** Remove lsp's from devshell - (a43d5d0) - Soispha
#### Features
- **(src/clean)** Add command to correctly quote the input for shells - (56fb0bd) - Soispha

- - -

## v1.9.0 - 2023-12-19
#### Features
- **(clean)** Correct awk source file behavior - (d9a29fe) - Soispha

- - -

## v1.8.0 - 2023-12-12
#### Features
- **(tempfiles)** Add `tmp_pipe` function - (042d232) - Soispha
#### Tests
- Add Tests - (66317f6) - Soispha

- - -

## v1.7.1 - 2023-11-25
#### Bug Fixes
- **(tempfiles/tmp)** Support pipes ("|") - (813c343) - Soispha

- - -

## v1.7.0 - 2023-11-25
#### Bug Fixes
- **(completions/generate_completions.sh)** Remove debug parts - (4780112) - Soispha
- **(tempfiles/ptmp)** Sanitize input (use %s) - (7571fe4) - Soispha
#### Build system
- **(flake)** Update - (71b7edc) - Soispha
#### Features
- **(completion)** Support arguments for options - (9761c97) - Soispha

- - -

## v1.6.4 - 2023-10-25
#### Bug Fixes
- **(src/nix)** Create output dir before using it - (629d7d7) - Soispha

- - -

## v1.6.3 - 2023-10-25
#### Bug Fixes
- **(src/nix)** Specify '-r' for cp - (4f8b746) - Soispha

- - -

## v1.6.2 - 2023-10-25
#### Bug Fixes
- **(src/nix)** Use the correct 'lib.optionals' instead of 'lib.optional' - (3d6bd68) - Soispha

- - -

## v1.6.1 - 2023-10-25
#### Bug Fixes
- **(src/lib.nix)** Point the cp to the correct path - (36a2db5) - Soispha

- - -

## v1.6.0 - 2023-10-25
#### Features
- **(src/completions)** Init the parsing scripts - (2e0e6d2) - Soispha
- **(src/nix)** Add support for the completion generation - (d39e607) - Soispha

- - -

## v1.5.0 - 2023-10-20
#### Bug Fixes
- **(lib/nix)** Add 'gnugrep' as dependency - (4a19e9b) - Soispha
#### Build system
- **(flake)** Update - (47b8924) - Soispha
#### Features
- **(lib/sh/alias)** Add a function to turn on 'set -x', for full debug - (c66d780) - Soispha

- - -

## v1.4.2 - 2023-09-16
#### Bug Fixes
- **(tempfiles)** Check correct variable for spaces - (6ff013b) - Soispha

- - -

## v1.4.1 - 2023-09-16
#### Bug Fixes
- **(aliases)** Make the dbg enable/disable aliases naming more obvious - (ea5c6c0) - Soispha
- **(treewide)** Replace old `tmp`, and add deprecation notice - (7033ea4) - Soispha

- - -

## v1.4.0 - 2023-09-16
#### Bug Fixes
- **(tempfiles/tmp)** Remove `eval` to avoid word breakage - (5a44840) - Soispha
#### Build system
- **(flake)** Update - (e6b8a68) - Soispha
#### Features
- **(dir_search)** Abstract to allow specified file names - (9e62da4) - Soispha
#### Miscellaneous Chores
- **(version)** v1.3.1 - (56f2ae0) - Soispha

- - -

## v1.3.1 - 2023-09-05
#### Bug Fixes
- **(flake/update_shell_library)** Use correct regex - (1ce6a68) - Soispha

- - -

## v1.3.0 - 2023-09-05
#### Features
- **(flake)** Add app output to automatically update to newest version - (6e45b4e) - Soispha

- - -

## v1.2.0 - 2023-09-05
#### Build system
- **(flake)** Update - (9161b45) - Soispha
#### Features
- **(tempfiles)** Add a `ptmp` function to support a common pattern - (ad7dfb6) - Soispha

- - -

## v1.1.4 - 2023-05-28
#### Bug Fixes
- **(template)** Add vim filetype directive - (021aef7) - Soispha

- - -

## v1.1.3 - 2023-05-28
#### Bug Fixes
- **(src/lib.nix)** Substitute after installing - (791e77a) - Soispha
#### Build system
- **(flake)** Update - (979e968) - Soispha

- - -

## v1.1.2 - 2023-05-28
#### Bug Fixes
- **(src/lib.nix)** Rework baseFunction - (f9ddc43) - Soispha

- - -

## v1.1.1 - 2023-05-28
#### Bug Fixes
- **(flake)** Add lib as an argument - (20ee586) - Soispha
#### Build system
- **(cog)** Run `nix flake check` to ensure correctness - (ca135a2) - Soispha

- - -

## v1.1.0 - 2023-05-28
#### Features
- **(src/lib.nix)** Add an option to allow for replacementStrings - (a2bb2c4) - Soispha

- - -

## v1.0.1 - 2023-05-27
#### Bug Fixes
- **(src/lib.nix)** Improve deprecation notices - (3892bd3) - Soispha

- - -

## v0.5.0 - 2023-05-27
#### Bug Fixes
- **(src/lib.nix)** Specify missing dependencies - (9cc5ba6) - Soispha
- **(src/lib.nix)** Rename 'script' argument to 'src' - (804c1ee) - Soispha
- **(src/lib.nix)** Rename functions to comply with nixpkgs naming - (9ce0ba0) - Soispha
- **(src/shell/check_versions)** Correctly pass the version string around - (b41a956) - Soispha
#### Build system
- **(flake)** Update - (b1b3306) - Soispha
- **(flake)** Add automatic versioning of the flake.nix - (580c636) - Soispha
#### Documentation
- **(src/shell/clean)** Add comment for further reading - (020b8ea) - Soispha
#### Features
- **(src/lib.nix)** Add the required variables for the version function - (165e71c) - Soispha
- **(template)** Add a template - (3a68d34) - Soispha
- **(treewide)** Support version checking - (71a5c9a) - Soispha
#### Performance Improvements
- **(src/shell/clean)** Avoid allocation a tempfile - (5192000) - Soispha
#### Refactoring
- **(src/shell)** Sort scripts with numbers - (af9bc79) - Soispha
#### Style
- **(src/lib.nix)** Format - (3d9639f) - Soispha

- - -

## v0.4.2 - 2023-05-26
#### Bug Fixes
- **(scr/tempfiles)** Remove wrong deprecation warning - (f5ecf9c) - Soispha
#### Build system
- **(flake)** Update - (4d1e644) - Soispha

- - -

## v0.4.1 - 2023-05-21
#### Bug Fixes
- **(tempfiles)** Set the temp dir variable before subshells - (009f855) - Soispha

- - -

## v0.4.0 - 2023-05-18
#### Features
- **(lib.sh)** Add version function - (a3556ec) - Soispha
#### Refactoring
- **(lib.sh)** Remove main.sh, as it was not used - (1856693) - Soispha

- - -

## v0.3.0 - 2023-05-18
#### Bug Fixes
- **(lib.sh)** Warn on outdated script - (25267ae) - Soispha
#### Features
- **(lib.sh)** Allow to pass arguments to `mktemp` - (3c27a0e) - Soispha
#### Refactoring
- **(lib.sh)** Separate input from output functions - (491d757) - Soispha

- - -

## v0.2.3 - 2023-05-13
#### Bug Fixes
- **(flake)** Add full path to the outputs, not only the dir - (6232f34) - Soispha
- **(lib.nix)** Add fd as dependency - (4127db2) - Soispha
#### Miscellaneous Chores
- **(version)** v0.2.2 - (4b1e30d) - Soispha

- - -

## v0.2.2 - 2023-05-13
#### Bug Fixes
- **(flake)** Add full path to the outputs, not only the dir - (b1828f9) - Soispha

- - -

## v0.2.1 - 2023-05-13
#### Bug Fixes
- **(exit)** Readd, as this was lost in the refactor - (7cedad7) - Soispha

- - -

## v0.2.0 - 2023-05-13
#### Bug Fixes
- **(output)** Print correct output, not a line per string - (cf28bba) - Soispha
#### Build system
- **(cog)** Rename changelog to CHANGELOG.md - (a571d74) - Soispha
#### Features
- **(dir_search)** Add a function to get the flake base dir - (8d68242) - Soispha
#### Miscellaneous Chores
- **(version)** v0.1.0 - (dcf87ce) - Soispha

- - -

## v0.1.0 - 2023-05-13
#### Bug Fixes
- **(flake)** Add derivation - (d883997) - Soispha
- **(flake)** Fix typo - (ebae838) - Soispha
- **(update)** Remove, as it's handled differently - (1665a0b) - Soispha
- Remove tempdir creation - (3243447) - ene
- Remove the temp directory directly in the lib - (3dd0f5f) - ene
- Remove unneeded /dev/tty - (c790198) - ene
- Use clearer name - (49aa259) - ene
#### Build system
- **(cog)** Add - (b07ecf9) - Soispha
#### Documentation
- **(manpage)** Add - (1824f8b) - Soispha
#### Features
- **(flake)** Expose raw lib as output - (1630178) - Soispha
- **(scr/lib)** Add further aliases for an easier api - (bc031e6) - Soispha
- **(scr/lib)** Add debug command - (7f82a01) - Soispha
- **(scr/lib.sh)** Add dbg2 for better selection when debugging - (04d5240) - Soispha
- **(src/lib.sh)** Make debug blue - (8dc90f2) - Soispha
- **(treewide)** Update to Nix - (4769a7e) - Soispha
- **(update)** Add script to remember updating - (0b8875d) - Soispha
- Change api from 'die', giving the option to add exit code - (f67e0f9) - ene
- Generate the temp dir directly in the lib - (3d6ce20) - ene
- Separate temp files per script - (7ec5e4d) - ene
#### Refactoring
- **(lib.sh)** Separate shell library in multiple file - (0e24005) - Soispha
- **(treewide)** Merge lib and scr directory - (d0f017f) - Soispha
- Outsource awk code and better error handling - (6cc6c37) - ene
- Give functions better names and use them - (43f79d3) - ene
#### Style
- Format and remove useless newlines - (314d9a5) - ene

- - -

Changelog generated by [cocogitto](https://github.com/cocogitto/cocogitto).