{
    if (NF) {
        # Strip leading and trailing whitespace
        gsub(/^[[:blank:]]*/, "", $0)
        gsub(/[[:blank:]]*$/, "", $0)
        # Shell like comments
        if (! /^#/ || /^#!/) {
            if (/.*#/ && ! /#!/) {
                gsub(/#.*$/, "", $0)
                gsub(/[[:blank:]]*$/, "", $0)
                if (NF) {
                    check_c_like()
                }
            } else {
                check_c_like()
            }
        }
    }
}


function check_c_like()
{
    comment = match($0, "^//")
    if (comment == 0) {
        print $0
    }
}
