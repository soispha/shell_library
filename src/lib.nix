{
  pkgs,
  shell_library,
  lib,
  ...
}: let
  shellLibraryDeps =
    builtins.attrValues {inherit (pkgs) gawk coreutils mktemp dash fd gnugrep;};
  completionDependencies = with pkgs; [
    jq # json manipulation
    hjson # used to turn awk's output to real json
  ];

  deprecated = import ./deprecated.nix;

  default_args = name: {
    checkPhase = ''
      # Run dash in dryRun mode
      ${lib.getExe pkgs.dash} -n "$out/bin/${name}"
    '';
  };

  getShellDependencies = {
    dependencies,
    generateCompletions,
  }:
    shellLibraryDeps
    ++ dependencies
    ++ lib.optionals generateCompletions
    completionDependencies;

  getSubstituteCommandRaw = {replacementStrings}:
    if replacementStrings != null
    then let
      strings =
        lib.attrsets.mapAttrsToList (string: value: ''--replace-fail "%${string}" "${value}"'') replacementStrings;
    in
      strings
    else '''';
  getCompletionCommand = {
    generateCompletions,
    name,
  }:
    if generateCompletions
    then ''
      "$out/bin/${name}" --help > ./help;
      cp -r ${./completions/awk} ./awk
      cp -r ${./completions/jq} ./jq
      mkdir --parents "$out/share/zsh/site-functions"
      ${./completions/generate_completions.sh} ./help "${name}" > "$out/share/zsh/site-functions/_${name}"
    ''
    else "";

  getSubstituteCommand = {
    replacementStrings,
    name,
  }: let
    substituteCommandRaw = getSubstituteCommandRaw {inherit replacementStrings;};
  in
    if replacementStrings != null
    then ''
      substituteInPlace "$out/bin/${name}" ${lib.strings.concatStringsSep " " substituteCommandRaw}
    ''
    else '''';

  getWrapCommand = {
    wrap,
    keepPath,
    name,
    shellDependencies,
    version,
  }: let
    pathSetting =
      if keepPath
      then "--prefix PATH :"
      else "--set PATH";
  in
    if wrap
    then ''
      wrapProgram "$out/bin/${name}" ${pathSetting} ${pkgs.lib.makeBinPath shellDependencies} --set NAME "${name}" --set VERSION "${version}";
    ''
    else "";

  getRunInput = {
    dependencies,
    name,
    src,
    version,
    replacementStrings,
    generateCompletions,
    keepPath,
    wrap,
  }: let
    shellDependencies = getShellDependencies {inherit dependencies generateCompletions;};

    substituteCommand = getSubstituteCommand {inherit replacementStrings name;};

    completionCommand = getCompletionCommand {inherit generateCompletions name;};

    wrapCommand = getWrapCommand {inherit name wrap shellDependencies version keepPath;};
  in
    ''
      install -m755 ${src} -D "$out/bin/${name}"
      substituteInPlace "$out/bin/${name}" --replace-fail "%SHELL_LIBRARY_PATH" "${shell_library}"
      patchShebangs "$out/bin/${name}"
    ''
    + substituteCommand
    + completionCommand
    + wrapCommand;
in
  {
    writeShellScript = {
      # NOTE: These can be empty, if we assume unwrapped <2024-01-13>
      dependencies ? [],
      name,
      src,
      version ? "unversioned",
      replacementStrings ? null,
      generateCompletions ? false,
      # FIXME: This does not remove the script dependencies (injected into path) after execution
      keepPath ? false,
      # FIXME: Setting this to false makes calling version() impossible, as NAME and VERSION are not set
      wrap ? true,
    }: let
      shellDependencies = getShellDependencies {inherit dependencies generateCompletions;};
      input = getRunInput {
        inherit
          src
          dependencies
          name
          version
          replacementStrings
          generateCompletions
          keepPath
          wrap
          ;
      };
    in
      pkgs.runCommandLocal "${name}" ({
          nativeBuildInputs = shellDependencies ++ lib.optional wrap pkgs.makeWrapper;
        }
        // default_args name)
      input;

    writeShellScriptMultiPart = {
      baseName,
      cmdPrefix,
      cmdNames,
      # NOTE: These can be empty, if we assume unwrapped <2024-01-13>
      dependencies ? [],
      name,
      src,
      version ? "unversioned",
      replacementStrings ? null,
      generateCompletions ? false,
      # FIXME: This does not remove the script dependencies (injected into path) after execution
      keepPath ? false,
      # FIXME: Setting this to false makes calling version() impossible, as NAME and VERSION are not set
      wrap ? true,
    }: let
      shellDependencies = getShellDependencies {inherit dependencies generateCompletions;};
      substituteCommandRaw = getSubstituteCommandRaw {inherit replacementStrings;};

      partSubstitution =
        (lib.strings.concatStringsSep "\n"
          (builtins.map (
              name: ''sed -i -e '/. .\/${cmdPrefix}\/${name}/{r ./${cmdPrefix}/${name}' -e 'd;}' ./${baseName} ''
            )
            cmdNames))
        + "\n";

      input = getRunInput {
        inherit
          dependencies
          name
          version
          replacementStrings
          generateCompletions
          keepPath
          wrap
          ;
        src = "./${baseName}";
      };
    in
      pkgs.stdenv.mkDerivation ({
          inherit name version src;
          nativeBuildInputs = shellDependencies ++ lib.optional wrap pkgs.makeWrapper;
          buildPhase =
            partSubstitution
            + input;
        }
        // default_args name);
  }
  // deprecated
# vim: ts=2

