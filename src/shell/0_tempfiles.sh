#! /bin/sh

mktmp() {
    ensure_tmp_dir;
    mktemp -p "$SHELL_LIBRARY_TEMP_PREFIX_DIR" "$@";
}

ensure_tmp_dir() {
    if ! [ -d "$SHELL_LIBRARY_TEMP_PREFIX_DIR" ];then
        SHELL_LIBRARY_TEMP_PREFIX_DIR="$(mktemp -d)";
    fi
};

# A new version of tmp, which allows you to specify the commandline args as normal
# arguments
tmp() {
    if echo "$1" | grep -E ' ' -q; then
        warn "Detected an old version of tmp, as there are spaces in the input string!"
    fi

    TEMP_DIR_WITH_DEL="$(mktmp)";
    "$@" 1> "$TEMP_DIR_WITH_DEL";
    echo "$TEMP_DIR_WITH_DEL";
};

# A version of tmp that accepts pipes as input in commands ("|"), does however not work
# with quoted input (all input is unquoted to be able to interpret the pipes)
tmp_pipe() {
    TEMP_DIR_WITH_DEL="$(mktmp)";
    eval "$*" 1> "$TEMP_DIR_WITH_DEL";
    echo "$TEMP_DIR_WITH_DEL";
};

ptmp() {
    tmp printf "%s" "$@"
}
etmp() {
    tmp printf "%s\n" "$@"
}

remove_tmp_dir() {
    # The test is here because some scripts still delete this on their own
    if [ -d "$SHELL_LIBRARY_TEMP_PREFIX_DIR" ];then
        rm -r "$SHELL_LIBRARY_TEMP_PREFIX_DIR";
    else
        warn "Using a deprecated shell lib version";
    fi
}



trap remove_tmp_dir EXIT;
ensure_tmp_dir; # ensure that the variable has been set, even in subshells
