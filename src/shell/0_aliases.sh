#! /bin/sh

# aliases for easier api
warn() {
    warning "$*";
}
dbg() {
    debug "$*"
}
dbg2() {
    debug2 "$*"
}
dbg_enable() {
    export SHELL_LIBRARY_DEBUG=true;
}
dbg_disable() {
    unset SHELL_LIBRARY_DEBUG;
}
dbg_enable_full() {
    export SHELL_LIBRARY_FULL_DEBUG=true;
}
dbg_disable_full() {
    unset SHELL_LIBRARY_FULL_DEBUG;
    set +x
}
[ "$SHELL_LIBRARY_FULL_DEBUG" ] && set -x
