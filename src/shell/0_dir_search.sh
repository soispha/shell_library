#! /bin/sh

# Takes a path to search as argument.
# Also takes further arguments as names to search for.
# Prints the path to a file, if a file matching the given name is found.
search_dir_for_file() {
    [ -d "$1" ] || die "Arg $1 is not a directory";
    directory="$1" && shift 1;

    while read -r dir_entry; do
        dir_entry_count=$((dir_entry_count + 1));
        dir_entry_basename="$(basename "$dir_entry")";

        dbg2 "checking file: $dir_entry";
        for name in "$@";do
            if [ "$name" = "$dir_entry_basename" ]; then
                println "$dir_entry";
                dbg2  "$dir_entry matches";
            fi
        done
    done < "$(tmp fd . "$directory" --type file --max-depth 1)"
    print "";
}


# Searches upward for the flake base dir as indicated by flake.lock and flake.nix
# Prints a path to the dir if it exists, otherwise nothing is returned
search_flake_base_dir() {
    search_upward_files "flake.lock" "flake.nix"
}

# Returns the path to the directory which contains all the specified files:
# `search_upward_files file1 file2 file3` returns a path containing file1 ...
search_upward_files() {
    directory="$(pwd)";
    final_directory="";
    output="$(mktmp)";
    while [ "$(wc -l < "$output")" = 0 ]; do
        dbg "searching dir for files: $directory";
        search_dir_for_file "$directory" "$@" > "$output";
        directory="$(dirname "$directory")";
        if [ "$directory" = "/" ]; then
            warning "We bailed out, as there seems to be no directory containing $*";
            final_directory="";
            return;
        fi
    done
    dbg "flake search done";
    final_directory="$(dirname "$(head -n1 "$output")")";
    dbg "Final dir: $final_directory";
    print "$final_directory";
}
