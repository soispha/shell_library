#! /bin/sh

SHELL_LIBRARY_MAJOR_VERSION="$(echo "$SHELL_LIBRARY_VERSION" | awk 'BEGIN{FS="."} {print $1}')";
SHELL_LIBRARY_MINOR_VERSION="$(echo "$SHELL_LIBRARY_VERSION" | awk 'BEGIN{FS="."} {print $2}')";
SHELL_LIBRARY_PATCH_VERSION="$(echo "$SHELL_LIBRARY_VERSION" | awk 'BEGIN{FS="."} {print $3}')";

if [ "$SHELL_LIBRARY_MAJOR_VERSION" != "%SHELL_LIBRARY_MAJOR_VERSION" ]; then
    die "The major version of the library (%SHELL_LIBRARY_MAJOR_VERSION) and the expected one ($SHELL_LIBRARY_MAJOR_VERSION) do not match!";
elif [ "$SHELL_LIBRARY_MINOR_VERSION" != "%SHELL_LIBRARY_MINOR_VERSION" ]; then
    warn "The minor version of the library (%SHELL_LIBRARY_MINOR_VERSION) and the expected one ($SHELL_LIBRARY_MINOR_VERSION) do not match, setting 'set -euf' to avoid mistakes! Please update";
    set -euf;
elif [ "$SHELL_LIBRARY_PATCH_VERSION" != "%SHELL_LIBRARY_PATCH_VERSION" ]; then
    warn "The patch version of the library (%SHELL_LIBRARY_PATCH_VERSION) and the expected one ($SHELL_LIBRARY_PATCH_VERSION) do not match; please update";
fi
