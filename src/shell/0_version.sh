#! /bin/sh

version(){
if [ -z "$NAME" ] || [ -z "$VERSION" ] || [ -z "$AUTHORS" ] || [ -z "$YEARS" ];then
    die "One of the following required variables is not defined: NAME or VERSION or AUTHORS or YEARS!";
fi

cat << EOF
This is $NAME, with the version $VERSION.

$NAME Copyright (C) $YEARS $AUTHORS
This program comes with ABSOLUTELY NO WARRANTY.
This is free software, and you are welcome to redistribute it
under certain conditions.
EOF
}
