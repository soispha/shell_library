#! /bin/sh

# clean shell like comments
clean() {
    awk --file "%AWK_CLEAN_FILE" "$@"
}

# clean and send back text inside of tags
clean_in_tag() {
    if [ "$2" ]; then
        clean "$2" | awk -v ro="$1" 'BEGIN { RS=ro } {if(NR % 2==0) {print $0}}' | clean
    else
        clean | awk -v ro="$1" 'BEGIN { RS=ro } {if(NR % 2==0) {print $0}}' | clean
    fi
}

quotify() {
    # The quote escaping is correct, see the tests
    # shellcheck disable=1003
    awk '{ gsub(/'\''/, "'\''\\'\'''\''", $0); print("'\''" $0 "'\''"); }' "$@"
}

#TODO clean c like comments
# See https://web.mit.edu/gnu/doc/html/gawk_5.html#SEC29  for a possible solution
#
# See:
# https://ahmedsharifalvi.wordpress.com/2013/02/01/detecting-and-removing-c-style-comment-using-awk/
# for a working version.
#
#clean_c() { awk '{
#    if (NF) {
#        if (!/^\/\//) {
#            if (!/^[[:blank:]]*#/) {
#                print $0
#            }
#        }
#    }
#}' "$1"; }
