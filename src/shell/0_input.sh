#! /bin/sh
if [ -n "$NO_COLOR" ];then
    readp() {
        prompt "$1";
        read -r "$2";
    };
else
    readp() {
        prompt "$1";
        read -r "$2";
    };
fi
