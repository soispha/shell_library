#! /bin/sh

die() {
    error "$1";
    if [ -n "$2" ]; then
        exit "$2";
    else
        exit 1;
    fi
}

# runs the cmd provided by "$1" until it succeeds, if it fails it runs "$2".
# "$2" is meant as a way to add a failure massage.
do_until_success() {
    eval "$1";
    response=$?;
    while [ $response -ne 0 ];do
        eval "$2";
        eval "$1";
        response=$?;
    done
}

