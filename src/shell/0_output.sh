#! /bin/sh

print() {
    printf "$*";
};
println() {
    printf "$*\n";
};
eprint() {
    >&2 print "$@";
};
eprintln() {
    >&2 println "$@";
};


if [ -n "$NO_COLOR" ];then
    error() {
        eprintln "==> ERROR:" "$*";
    };
    warning() {
        eprintln "==> WARNING:" "$*";
    };
    debug() {
        [ -n "$SHELL_LIBRARY_DEBUG" ] && eprintln "==> [Debug:]" "$*";
    };
    debug2() {
        [ -n "$SHELL_LIBRARY_DEBUG" ] && eprintln " -> [Debug:]" "$*";
    };
    msg() {
        eprintln "==>" "$*";
    };
    msg2() {
        eprintln " ->" "$*";
    };
    prompt() {
        eprint "..>" "$*";
    };
else
    # See https://stackoverflow.com/a/33206814 for ansi codes
    error() {
        eprintln "\033[1;91m==> ERROR:\033[0m" "\033[1;93m$*\033[0m";
    };
    warning() {
        eprintln "\033[1;91m==> WARNING:\033[0m" "\033[1;93m$*\033[0m";
    };
    debug() {
        [ -n "$SHELL_LIBRARY_DEBUG" ] && eprintln "\033[1;94m==> [Debug:]\033[0m" "\033[1;93m$*\033[0m";
    };
    debug2() {
        [ -n "$SHELL_LIBRARY_DEBUG" ] && eprintln "\033[1;94m -> [Debug:]\033[0m" "\033[1;93m$*\033[0m";
    };
    msg() {
        eprintln "\033[1;96m==>\033[0m" "\033[1;93m$*\033[0m";
    };
    msg2() {
        eprintln "\033[1;96m ->\033[0m" "\033[1;93m$*\033[0m";
    };
    prompt() {
        eprint "\033[1;96m..>\033[0m" "\033[1;93m$*\033[0m";
    };
fi
