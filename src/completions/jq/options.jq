.options |
map (
        {
cmd: .option_long,
desc: .option_description | join ("") | @html
}
) |
map(.cmd + ":'" + .desc + "'") |
"_describe 'command' \"(" + join(" ") + ")\""

#vim: jq
