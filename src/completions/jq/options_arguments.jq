# The strings contain indention
.options |
map(
    if (.option_arguments | length > 0) then
        "            \(.option_long) | \(.option_short))\n" +
        (.option_arguments |   map (
            if (.optional == false) then
             "                completing_option=1\n"
            else
             "                completing_option=0\n"
            end
             + "                _\(.name)\n"
             ) | join("") )   + "            ;;"
    else "" end

   ) | join("\n")

