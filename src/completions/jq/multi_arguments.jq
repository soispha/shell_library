# The strings contain indention
.commands |
map(
    if (.command_arguments | length > 0) then
        "        \(.command_name))\n            case \"${line[2]}\" in\n" +

        (.command_arguments |   map (
             "                %MATCH_\(.name | ascii_upcase))\n                    _\(.name)" + "\n                    " + if (.optional) then ";&\n" else ";;\n" end
             ) | join("") )   + "            esac\n            ;;"
    else "" end

   ) | join("\n")

