.arguments | map(
        "function _\(.argument_name)()\n{\n" + "    " + if (.argument_function) then
        "_describe 'command' \"($( \(.argument_function) ))\""
        else "compadd " + (.argument_values | join(" ")) end +
        "\n}\n"
        ) | join("")
