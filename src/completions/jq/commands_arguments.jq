# The strings contain indention
.commands |
map(
    if (.command_arguments | length > 0) then
        "            \(.command_name))\n" +

        (.command_arguments |   map (
             "                _\(.name)\n"
             ) | join("") )   + "            ;;"
    else "" end

   ) | join("\n")

