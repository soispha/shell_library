BEGIN {
    print "{"
}

{
    if ($0 ~ /^\s*$/) {
        is_description = 0
        next
    }
    switch ($0) {
    case "USAGE:":
        print_header_obj("USAGE")
        mode = "usage"
        is_description = 0
        reached_usage = 1
        next
    case "OPTIONS:":
        if (! reached_usage) {
            print_header_obj("USAGE")
            print "}"
        }
        print_header("OPTIONS")
        mode = "options"
        is_description = 0
        reached_options = 1
        next
    case "COMMANDS:":
        if (! reached_options) {
            if (! reached_usage) {
                print_header_obj("USAGE")
                print "}"
            }
            print_header("OPTIONS")
            print "{"
            json_end_list()
        }
        print_header("COMMANDS")
        mode = "commands"
        is_description = 0
        reached_commands = 1
        next
    case "ARGUMENTS:":
        if (! reached_commands) {
            if (! reached_options) {
                if (! reached_usage) {
                    print_header_obj("USAGE")
                    print "}"
                }
                print_header("OPTIONS")
                print "{"
                json_end_list()
            }
            print_header("commands")
            print "{"
            json_end_list()
        }
        print_header("ARGUMENTS")
        mode = "arguments"
        is_description = 0
        reached_arguments = 1
        next
    }
    switch (mode) {
    case "arguments":
        if (is_description == 1) {
            json_init_list("argument_description")
            gsub(/^[ \t]+|[ \t]+$/, "", $0)
            json_print_list($0)
        } else {
            json_end_list()
            print_seperator()
            is_description = 1
            FS = ":="

            # parse argument_match
            gsub(/^[ \t]+|[ \t]+$/, "", $0)
            orig = $0
            FS = "|"
            $0 = $1
            gsub(/^[ \t]+|[ \t]+$/, "", $1)
            gsub(/^[ \t]+|[ \t]+$/, "", $2)
            json_print("argument_name", $1)
            if ($2) {
                json_print("argument_match", $2)
            }

            FS = ":="
            $0 = orig
            gsub(/^[ \t]+|[ \t]+$/, "", $1)
            gsub(/^[ \t]+|[ \t]+$/, "", $2)
            if ($2 ~ /\[\[.*\]\]/) {
                gsub(/^\[\[|\]\]$/, "", $2)
                json_print("argument_function", $2)
            } else {
                FS = "|"
                $0 = $2
                json_init_list("argument_values")
                for (i = 1; i <= NF; i++) {
                    print json_single($i)
                }
                json_end_list()
            }
        }
        break
    case "commands":
        if (is_description == 1) {
            json_init_list("command_description")
            gsub(/^[ \t]+|[ \t]+$/, "", $0)
            json_print_list($0)
        } else {
            json_end_list()
            print_seperator()
            is_description = 1
            gsub(/^[ \t]+|[ \t]+$/, "", $0)
            json_print("command_name", $1)
            json_init_list("command_arguments")
            for (i = 2; i <= NF; i++) {
                if (match($i, /\[.*\]/)) {
                    optional_args = 2
                } else if (match($i, /\[.*/)) {
                    optional_args = 1
                } else if (match($i, /.*\]/)) {
                    optional_args = 2
                }
                gsub(/\[|\]/, "", $i)
                switch (optional_args) {
                case 2:
                    optional_args = 0
                case 1:
                    obj["optional"] = "true"
                    obj["name"] = $i
                    json_print_list_raw(json_object(obj))
                    break
                case 0:
                    obj["optional"] = "false"
                    obj["name"] = $i
                    json_print_list_raw(json_object(obj))
                    break
                }
            }
            json_end_list()
            FS = " "
        }
        break
    case "options":
        if (is_description == 1) {
            json_init_list("option_description")
            gsub(/^[ \t]+|[ \t]+$/, "", $0)
            json_print_list($0)
        } else {
            json_end_list()
            print_seperator()
            is_description = 1
            FS = "|"
            gsub(/^[ \t]+|[ \t]+$/, "", $0)
            json_print("option_long", $1)
            FS = " "
            $0 = $2
            json_print("option_short", $1)
            $0 = $2
            json_init_list("option_arguments")
            for (i = 1; i <= NF; i++) {
                if (match($i, /\[.*\]/)) {
                    optional_args = 2
                } else if (match($i, /\[.*/)) {
                    optional_args = 1
                } else if (match($i, /.*\]/)) {
                    optional_args = 2
                }
                gsub(/\[|\]/, "", $i)
                switch (optional_args) {
                case 2:
                    optional_args = 0
                case 1:
                    obj["optional"] = "true"
                    obj["name"] = $i
                    json_print_list_raw(json_object(obj))
                    break
                case 0:
                    obj["optional"] = "false"
                    obj["name"] = $i
                    json_print_list_raw(json_object(obj))
                    break
                }
            }
            json_end_list()
        }
        break
    case "usage":
        json_print("bin_name", $1)
        json_init_list("file_args")
        for (i = 1; i <= NF; i++) {
            if (match($i, /[^\.]*\.\./)) {
                gsub(/\[|\]|\./, "", $i)
                json_print_list($i)
            }
        }
        json_end_list()
        break
    }
}

END {
    if (mode) {
        json_end_list()
        print "}"
        print "]" " // " tolower(mode)
        print ""
    }
    print "}"
}


function json_end_list(name)
{
    if (json_list_initialized) {
        print "],"
        json_list_initialized = 0
    }
}

function json_escape(value)
{
    gsub(/^[ \t]+|[ \t]+$/, "", value)
    gsub(/"/, "\\\"", value)
    return value
}

function json_init_list(name)
{
    if (! json_list_initialized) {
        print json_single(name) ": ["
        json_list_initialized = 1
    }
}

function json_object(obj)
{
    output = "{"
    for (key in obj) {
        value = json_single(obj[key])
        key = json_single(key)
        output = output key ":" value ","
    }
    output = output "}"
    return output
}

function json_print(name, arg)
{
    name = json_escape(name)
    arg = json_escape(arg)
    print "\"" name "\":" "\"" arg "\","
}

function json_print_list(name)
{
    print json_single(name) ", "
}

function json_print_list_raw(name)
{
    print name ", "
}

function json_single(name)
{
    if (name == "true" || name == "false") {
        return name
    } else {
        name = json_escape(name)
        return ("\"" name "\"")
    }
}

function print_header(header)
{
    if (mode && mode != "usage") {
        json_end_list()
        print "}"
        print "]" " // " tolower(mode)
        print ""
    } else if (mode == "usage") {
        print "}" " // " tolower(mode)
    }
    print json_single(tolower(header)) ": ["
    header_printed = 1
}

function print_header_obj(header)
{
    if (mode) {
        print "}" " // " tolower(mode)
        print ""
    }
    print json_single(tolower(header)) ": {"
    header_printed = 1
}

function print_seperator()
{
    if (header_printed) {
        header_printed = 0
        print "{"
    } else {
        printf "},\n{\n"
    }
}

# vim: ft=awk
