
function normalize_string(string)
{
    gsub(/^"|"$/, "", string)
    gsub(/\\"/, "\"", string)
    gsub(/\\n/, "\n", string)
    return string
}
