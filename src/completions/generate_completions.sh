#!/bin/sh

norm() {
    awk -i ./awk/utils.awk '{print normalize_string($0)}'
}

help="$1"
command_name="$2"

parsed="$(mktemp)"
trap cleanup EXIT

awk --exec=./awk/parse_help.awk "$help" | hjson -j | jq > "$parsed"

options="$(jq -f ./jq/options.jq < "$parsed" | norm)"
commands="$(jq -f ./jq/commands.jq < "$parsed" | norm)"

commands_arguments="$(jq -f ./jq/commands_arguments.jq < "$parsed" | norm)"
options_arguments="$(jq -f ./jq/options_arguments.jq < "$parsed" | norm)"

arguments_completions="$(jq -f ./jq/arguments_completions.jq < "$parsed" | norm)"

file_args="$(jq -f ./jq/file_args.jq < "$parsed" | norm)"

cat << EOF
#compdef _$command_name $command_name
function _$command_name() {
    local context state state_descr line completing_option=0
    typeset -A opt_args

    # actual command completions
    $commands

    # collect the current line
    _arguments '*::arg:->args'

    # we only support on argument, so we break after one (for cmd arguments)
    if [ "\${#line[@]}" -lt 3 ]; then
        case "\${line[1]}" in
$commands_arguments
        esac
    fi

    # option arguments can be completed everywhere (thus use the second last element)
    case "\${line[-2]}" in
$options_arguments
    esac

    # only complete options, when not completing a mandatory argument to another option
    if [ "\$completing_option" = 0 ] && [[ "\${line[-1]}" == "-"* ]]; then
        $options
    fi

$file_args
}

$arguments_completions
EOF

cleanup() {
    rm "$parsed"
}

# vim: ft=sh
