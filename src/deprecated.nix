{
  makeShellScriptWithLibrary = {...}:
    builtins.throw "'makeShellScriptWithLibrary' has been deprecated. Use 'writeShellScriptWithLibrary' instead.";

  makeShellScriptWithLibraryAndKeepPath = {...}:
    builtins.throw "'makeShellScriptWithLibraryAndKeepPath' has been deprecated. Use 'writeShellScriptWithLibraryAndKeepPath' instead.";

  makeShellScriptWithLibraryUnwrapped = {...}:
    builtins.throw "'makeShellScriptWithLibraryUnwrapped' has been deprecated. Use 'writeShellScriptWithLibraryUnwrapped' instead.";


  writeShellScriptWithLibrary = {...}:
    builtins.throw "'writeShellScriptWithLibrary' has been deprecated. Use 'writeShellScript' instead.";

  writeShellScriptWithLibraryAndKeepPath = {...}:
    builtins.throw "'writeShellScriptWithLibraryAndKeepPath' has been deprecated. Use 'writeShellScript { keep_path = true; }' instead.";

  writeShellScriptWithLibraryUnwrapped = {...}:
    builtins.throw "'writeShellScriptWithLibraryUnwrapped' has been deprecated. Use 'writeShellScript { wrap = false; }' instead.";
}
