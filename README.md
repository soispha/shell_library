# Shell lib
A useful shell library.

## Update
Run
```bash
nix run .#update_shell_library
// or
nix run . // as update_shell_library is default
```
to update all version strings in the files below the current directory.

## Test
Run
```bash
nix flake check
```
to check the library (that is run the tests under `./tests`)
