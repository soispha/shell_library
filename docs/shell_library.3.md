% LIBSHELL(1) LIBSHELL 1.0.0
% Soispha
% May 2023

# NAME
LIBSHELL - provides convenience functions you would have written anyway

# SYNOPSIS
. &lt;path to **libshell**&gt;

# DESCRIPTION
The functions are layed out in the respective files.

# BUGS
Report bugs to <https://codeberg.org/soispha/shell_library>.

# COPYRIGHT
Copyright (C) 2023 Soispha

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
