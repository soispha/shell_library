# Testing harness
The test are run from the flake by executing `nix flake check`.
Every test (that is all files with a `.sh` extension in this directory) is run in the same
namespace.
Additionally, every test should set a `"$tmp_correct"` and a `"$tmp_tested"` variable to a tmp
file, which are then compared for equality.

# Check for parsed code
If you want to see, what zsh has put in the line array, add this to the completion script.
```
    echo "-1: '${line[-1]}'" >> out.sh
    echo " 1: '${line[1]}'" >> out.sh
    echo " 2: '${line[2]}'" >> out.sh
    echo " 3: '${line[3]}'" >> out.sh
    echo " 4: '${line[4]}'" >> out.sh
    echo " 5: '${line[5]}'" >> out.sh
    echo "----------------" >> out.sh
```
