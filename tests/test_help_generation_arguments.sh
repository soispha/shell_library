cd ./src/completions || die "BUG: (Dir does not exists)"
./generate_completions.sh ../../tests/help_data/help_arguments.input "neorg" > "$tmp_tested"
cd - || die "BUG: (Dir does not exists)"

tmp_correct="./tests/help_data/help_arguments.expected"
