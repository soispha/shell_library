cat << EOF | clean > "$tmp_tested"
# Main options
--audio youtube youtube-music slider-kz soundcloud bandcamp piped
--lyrics synced genius musixmatch azlyrics

# FFmpeg options
--ffmpeg ffmpeg
--threads 16
--bitrate auto

# Spotify options
--cache-path /home/soispha/.local/share/spotdl/.spotipy

# Output options
--preload
--format opus
--output {artists}_-_{title}
--print-errors
--save-errors "$DOWN_DIR/spotdl-errors.log"
--generate-lrc
--overwrite skip

# Misc options
--log-level INFO
EOF

cat << EOF > "$tmp_correct"
--audio youtube youtube-music slider-kz soundcloud bandcamp piped
--lyrics synced genius musixmatch azlyrics
--ffmpeg ffmpeg
--threads 16
--bitrate auto
--cache-path /home/soispha/.local/share/spotdl/.spotipy
--preload
--format opus
--output {artists}_-_{title}
--print-errors
--save-errors "$DOWN_DIR/spotdl-errors.log"
--generate-lrc
--overwrite skip
--log-level INFO
EOF
